﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Attributes : MonoBehaviour 
{    
    [SerializeField] private Player _player;    
    [SerializeField] private List<Text> _attributesText;

    private void Start()
    {        
        DisplayPlayerAttributes();
    }
   
    private void DisplayPlayerAttributes()
    {        
        int i = 0;
        foreach (KeyValuePair<AttributeType, int> pair in _player.AttributeList)
        {
            _attributesText[i].text = string.Format("{0}: {1}", pair.Key, pair.Value);
            i++;            
        }
    }
}
