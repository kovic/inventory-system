﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class Equip : MonoBehaviour 
{
    private EquipSlot[] _equipSlots;
    private EquipSlot _currentSlot;

    private void Start()
    {
        _equipSlots = GetComponentsInChildren<EquipSlot>();
    }

    public void EquipItem(EquipableItem item)
    {        
        FindSlotByType(item).OnItemEquiped(item);                
    }

    public bool IsSlotEmpty(EquipableItem item)
    {
        return FindSlotByType(item).IsEmpty;        
    }

    // Checks if item type corresponds to the equip slot type.
    private EquipSlot FindSlotByType(EquipableItem item)
    {
        foreach (EquipSlot slot in _equipSlots)
        {
            if (slot.SlotType == item.Type)
            {
                return slot;
            }
        }
        return null;
    }
}
