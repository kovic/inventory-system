﻿using UnityEngine.EventSystems;

public class EquipSlot : InventorySlot, IPointerClickHandler
{
    public EquipmentType SlotType;

    public void OnItemEquiped(EquipableItem item)
    {
        Item = item;
        _slotImage.sprite = item.Icon;
        IsEmpty = false;
    }

    // IPointerClickHandler interface function. Triggered on right button click on equip slot.
    new public void OnPointerClick(PointerEventData eventData)
    {
        if (eventData.button == PointerEventData.InputButton.Right)
        {
            EventTrigger();
        }
    }

    // Empty override because parent class functionality is not needed but the function is called in Start function so it must be overriden here to terminate the effect.
    protected override void AssignEvents()
    {
        return;    
    }
    
    // Function is called when mouse event is triggered and determines the actions that should be taken.
    protected override void EventTrigger()
    {
        if (!IsEmpty && InputController.Instance.Drop)
            OnItemDropped();
        else if (IsEmpty && InventoryController.Instance.IsInventoryItemSelected)
            OnItemEquip();
        else if (!IsEmpty && !InventoryController.Instance.IsInventoryItemSelected)
            OnItemUnequip();
    }

    private void OnItemEquip()
    {
        InventoryController.Instance.EquipSelectedItem(this, _rectTransform);
    }

    private void OnItemUnequip()
    {
        bool canUnequip = InventoryController.Instance.UnequipSelectedItem(Item as EquipableItem);
        if (canUnequip)
        {
            IsEmpty = true;
            _slotImage.sprite = null;
            Item = null;
        }
    }
}
