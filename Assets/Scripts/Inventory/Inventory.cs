﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Inventory : MonoBehaviour 
{
    // Array of all inventory slot components on canvas.
    private InventorySlot[] _inventorySlots;

    private void Awake()
    {
        _inventorySlots = GetComponentsInChildren<InventorySlot>();     
    }
    
    public bool AddItem(InventoryItem item)
    {
        InventorySlot emptySlot = GetFirstEmptySlot();
        if (emptySlot != null)
        {
            emptySlot.OnItemAdded(item);
            return true;
        }
        else
        {
            return false;
        }
    }

    private InventorySlot GetFirstEmptySlot()
    {
        foreach (InventorySlot slot in _inventorySlots)
        {
            if (slot.IsEmpty)
                return slot;
        }        
        return null;
    }
}
