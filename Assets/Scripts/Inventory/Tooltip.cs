﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class Tooltip : MonoBehaviour 
{
    private Text _infoText;
    private Image _itemImage;
    private Transform _transform;

    private void Start()
    {
        _transform = transform;
        _infoText = _transform.Find("ItemInfo").GetComponent<Text>();
        _itemImage = _transform.Find("ItemImage").GetComponent<Image>();
    }

    public void ShowItemInfo(InventoryItem item)
    {
        _itemImage.sprite = item.Icon;
        _infoText.text = string.Format("Name: " + item.Name);                    

    }

    public void ShowItemInfo(EquipableItem item)
    {
        _itemImage.sprite = item.Icon;
        _infoText.text = string.Format("Name: " + item.Name + "\n" + 
            "Type: " + item.Type + "\n" + 
            "Attack: " + item.Attack + "\n" +
            "Defense: " + item.Defense + "\n" +
            "Attribute: " + item.Attribute + "\n" +
            "Durability: " + item.MaxDurability + "\n");
    }
}
