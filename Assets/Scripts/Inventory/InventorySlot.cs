﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine;

public class InventorySlot : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler
{
    #region PROTECTED FILEDS
    protected Transform _transform;
    protected RectTransform _rectTransform;
    protected Transform _itemPlaceholder;
    protected GameObject _selectedOBject;
    protected Button _slotButton;
    protected Image _slotImage;
    #endregion

    #region PUBLIC PROPERTIES
    public InventoryItem Item { get; set; }    
    public bool IsEmpty { get; set; }
    #endregion

    #region AWAKE
    protected void Awake()
    {
        _transform = transform;
        _rectTransform = transform.GetComponent<RectTransform>();
        _itemPlaceholder = _transform.GetChild(0);
        _slotImage = _itemPlaceholder.GetComponent<Image>();
        _slotButton = GetComponent<Button>();
        IsEmpty = true;

        AssignEvents();
    }
    #endregion

    #region PUBLIC METHODS
    public void OnItemAdded(InventoryItem item)
    {
        Item = item;
        ItemInfo();
        _slotImage.sprite = item.Icon;
        IsEmpty = false;
    }

    // IPointerEnterHandler interface function implementation
    public void OnPointerEnter(PointerEventData eventData)
    {
        if (!IsEmpty)
            InventoryController.Instance.ShowTooltip(Item, _rectTransform);
    }

    // IPointerExitHandler interface function implementation
    public void OnPointerExit(PointerEventData eventData)
    {
        if (!IsEmpty)
            InventoryController.Instance.CloseTooltip();
    }

    public void ItemInfo()
    {
        Debug.Log(Item.Name + " added to inventory.");
    }

    // IPointerClickHandler interface function. Triggered on middle button click on equip slot.
    public void OnPointerClick(PointerEventData eventData)
    {
        if (eventData.button == PointerEventData.InputButton.Middle)
        {
            OnItemConsume();
        }
    }
    #endregion

    #region PROTECTED METHODS
    protected virtual void AssignEvents()
    {
        _slotButton.onClick.AddListener(EventTrigger);
    }

    // Function is called when mouse event is triggered and determines the actions that should be taken.
    protected virtual void EventTrigger()
    {
        if (!IsEmpty && InputController.Instance.Drop)
            OnItemDropped();
        else if (IsEmpty && InventoryController.Instance.IsInventoryItemSelected)
            OnItemSlotted();
        else if (!IsEmpty && !InventoryController.Instance.IsInventoryItemSelected)
            OnItemSelected();      
    }

    // handle dropping item on the floor
    protected void OnItemDropped()
    {
        if (InventoryController.Instance.CanDropItem())
        {
            IsEmpty = true;
            _slotImage.sprite = null;
            InventoryController.Instance.DropItem(Item);
            Item = null;
        }
    }
    #endregion

    #region PRIVATE METHODS
    // handle situation when item is put to slot after it was selected
    private void OnItemSlotted()
    {
        IsEmpty = false;
        Item = InventoryController.Instance.CurrentlySelectedItem;
        _slotImage.sprite = Item.Icon;        
        InventoryController.Instance.PutItemOnSlot(Item, _rectTransform);
    }

    // handle item selection
    private void OnItemSelected()
    {
        IsEmpty = true;
        _slotImage.sprite = null;        
        InventoryController.Instance.SelectItem(Item, _transform);
        Item = null;
    }

    // handle item consumation
    private void OnItemConsume()
    {
        Debug.Log(Item + " is consumed!");
        InventoryController.Instance.CloseTooltip();
        IsEmpty = true;
        _slotImage.sprite = null;
        Item = null;
    }
    #endregion
}
