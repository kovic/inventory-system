﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WarningWindow : MonoBehaviour 
{
    public void CloseWindow(GameObject window)
    {
        window.SetActive(false);                   
    }

    public void DisplayWarningMessage()
    {
        transform.GetComponent<Text>().text = "Item can't be picked up because your inventory is full. Consume, equip or drop items to free some space.";
    }
}
