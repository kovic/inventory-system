﻿using UnityEngine;

public class CameraController : MonoBehaviour
{    
    private Transform _playerTransform;
    private Transform _transform;

    private void Start()
    {
        _transform = transform;
        _playerTransform = GameManager.Instance.Player;
    }

    private void Update()
    {
        FollowPlayer();
    }

    private void FollowPlayer()
    {
        Vector3 newPosition = new Vector3(_playerTransform.position.x, _playerTransform.position.y, _transform.position.z);
        _transform.position = newPosition;
    }
}
