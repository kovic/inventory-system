﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine;

public class InventoryController : MonoBehaviour
{
    #region PUBLIC FIELDS
    public GameObject SelectedItemPrefab;
    public static InventoryController Instance = null;
    #endregion

    #region PRIVATE FIELDS
    private Transform _canvasTransform;
    // Inventory fields
    private GameObject _inventoryPanel;
    private GameObject _inventoryButton;
    private ScrollRect _itemScrollRect;
    private Inventory _inventory;
    // Equip fields
    private GameObject _equipPanel;
    private GameObject _equipButton;
    private Equip _equip;
    // Attribute fields
    private GameObject _attributesPanel;
    private GameObject _attributesButton;
    //Tooltip fields
    private GameObject _tooltipObject;
    private RectTransform _tooltipRect;
    private Tooltip _tooltip;
    // Warning window
    private GameObject _warningWindowObject;
    private WarningWindow _warningWindow;
    // Reference fields
    private GameObject _selectedItemObject;
    private Transform _selectedItemObjectTransform;
    private Transform _previousItemSlot;

    private float _tooltipPadding = 5f;
    private int _maxSlotNumber = 160;    
    #endregion

    #region PROPERTIES
    public InventoryItem CurrentlySelectedItem { get; set; }
    public bool IsInventoryItemSelected { get; set; }
    public int MaxSlotNumber
    {
        get { return _maxSlotNumber; }
    }
    #endregion

    #region AWAKE
    private void Awake()
    {
        if (Instance == null)
            Instance = this;
        else if (Instance != null)
            Destroy(gameObject);

        _canvasTransform = transform;
        // This is bad practice but some thing didn't work well if they weren't called in awake. I should investigate.
        _inventoryPanel = _canvasTransform.Find("InventoryPanel").gameObject;
        _inventoryButton = _canvasTransform.Find("InventoryButton").gameObject;
        _itemScrollRect = GameObject.Find("ItemScrollView").GetComponent<ScrollRect>();
        _inventory = GameObject.Find("ItemSlotList").GetComponent<Inventory>();
        _equip = GameObject.Find("EquipSlotList").GetComponent<Equip>();
        _equipPanel = _canvasTransform.Find("EquipPanel").gameObject;
        _equipButton = _canvasTransform.Find("EquipButton").gameObject;
        _attributesPanel = _canvasTransform.Find("AttributesPanel").gameObject;
        _attributesButton = _canvasTransform.Find("AttributesButton").gameObject;
        _tooltipObject = _canvasTransform.Find("Tooltip").gameObject;
        _warningWindowObject = _canvasTransform.Find("WarningWindow").gameObject;
    }
    #endregion

    #region START
    private void Start()
    {        
        _tooltip = _tooltipObject.GetComponent<Tooltip>();
        _tooltipRect = _tooltipObject.GetComponent<RectTransform>();        
        _warningWindow = _warningWindowObject.GetComponent<WarningWindow>();
        // deactivating all UI windows
        _inventoryPanel.SetActive(false);
        _equipPanel.SetActive(false);
        _attributesPanel.SetActive(false);
        _tooltipObject.SetActive(false);
        _warningWindowObject.SetActive(false);
    }
    #endregion

    #region UPDATE
    private void Update()
    {
        ToggleInventoryWindows();

        // Catch left mouse click and see if item is in the air and if should it be dropped
        if (Input.GetMouseButtonDown(0))
            DropCurrentlySelectedItem();      

        if (IsInventoryItemSelected)
            PinSelectedItemToMouseArrow();

        // TODO: Improve this 
        // If inventory windows are cloed, return selected item to its previous slot and close active tooltip
        if (!_inventoryPanel.activeSelf && !_equipPanel.activeSelf)           
        {
            ReturnItemToPreviousSlot();
            CloseTooltip();
        }        
    }
    #endregion

    #region PUBLIC METHODS
    /// <summary>
    /// Add picked up item to inventory or equip window.
    /// </summary>
    /// <param name="item"></param>
    /// <returns>Return false if all slots are full.</returns>
    public bool AddItemToInventory(InventoryItem item)
    {
        // If picked up item i equipable, try to equip it
        if (item is EquipableItem && _equip.IsSlotEmpty(item as EquipableItem))
        {
            _equip.EquipItem(item as EquipableItem);
            return true;
        }
        else
        {
            // Try to add item to inventory and if it is full, display warning window
            bool hasEmptySlot = _inventory.AddItem(item);
            if (hasEmptySlot)
                return true;
            else
            {
                CreateWarningWindow();
                return false;
            }
        }
    }

    /// <summary>
    /// Select item from the inventory and pick it up
    /// </summary>
    /// <param name="selectedItem">Item to pick up</param>
    /// <param name="previousSlotTransform">Slot from which item was picked up</param>
    public void SelectItem(InventoryItem selectedItem, Transform previousSlotTransform)
    {
        CloseTooltip();
        _selectedItemObject = Instantiate(SelectedItemPrefab, _canvasTransform);
        _selectedItemObjectTransform = _selectedItemObject.transform;
        _selectedItemObject.GetComponent<Image>().sprite = selectedItem.Icon;
        IsInventoryItemSelected = true;
        CurrentlySelectedItem = selectedItem;
        _previousItemSlot = previousSlotTransform;
    }

    public void PutItemOnSlot(InventoryItem item, RectTransform slotTransform)
    {
        // reset all references after item was placed in an empty slot
        ResetSelectedItemReferences();
        ShowTooltip(item, slotTransform);
    }

    public void EquipSelectedItem(EquipSlot targetSlot, RectTransform slotTransform)
    {
        if (CurrentlySelectedItem is EquipableItem)
        {
            EquipableItem item = CurrentlySelectedItem as EquipableItem;
            if (_equip.IsSlotEmpty(item) && targetSlot.SlotType == item.Type)
            {
                _equip.EquipItem(item);
                ResetSelectedItemReferences();
                ShowTooltip(item, slotTransform);
            }
        }
    }

    public bool UnequipSelectedItem(EquipableItem item)
    {
        bool hasEmptySlot = _inventory.AddItem(item);
        if (hasEmptySlot)
            CloseTooltip();
        return hasEmptySlot;
    }

    public bool CanDropItem()
    {
        return CurrentlySelectedItem == null;
    }

    public void DropItem(InventoryItem item)
    {
        CloseTooltip();
        GameManager.Instance.InstantiateInventoryObjectOnGround(item.Name);
    }

    public void ShowTooltip(InventoryItem item, RectTransform slotTransform)
    {
        if (CurrentlySelectedItem == null)
        {
            // Show tooltip on the right of the slot and handle cases where tooltip window is displayed out of the viewport. There should be a better way of doing this.
            Vector3[] slotCorners = new Vector3[4];
            slotTransform.GetWorldCorners(slotCorners);
            _tooltipRect.position = new Vector3(slotCorners[2].x + _tooltipPadding, slotCorners[2].y, _tooltipRect.position.z);
            Vector3[] tooltipCorners = new Vector3[4];
            _tooltipRect.GetWorldCorners(tooltipCorners);
            if (tooltipCorners[2].x > Screen.width)
                _tooltipRect.position = new Vector3(slotCorners[1].x - (tooltipCorners[2].x - tooltipCorners[1].x) - _tooltipPadding, _tooltipRect.position.y, _tooltipRect.position.z);
            if (tooltipCorners[0].y < 0)
                _tooltipRect.position = new Vector3(_tooltipRect.position.x, slotCorners[0].y + (tooltipCorners[1].y - tooltipCorners[0].y), _tooltipRect.position.z);

            // Show tool tip and display information
            _tooltipObject.SetActive(true);
            if (item is EquipableItem)
                _tooltip.ShowItemInfo(item as EquipableItem);
            else
                _tooltip.ShowItemInfo(item);
        }
    }

    public void CloseTooltip()
    {
        if (_tooltipObject.activeSelf)
            _tooltipObject.SetActive(false);
    }

    // Attached to on click evenet in inspector
    public void ActivateInventoryPanel()
    {
        _inventoryPanel.SetActive(true);
        _inventoryButton.SetActive(false);
    }

    // Attached to on click evenet in inspector
    public void ActivateEquipPanel()
    {
        _equipPanel.SetActive(true);
        _equipButton.SetActive(false);
    }

    // Attached to on click evenet in inspector
    public void CloseInventoryPanel()
    {
        _inventoryPanel.SetActive(false);
        _inventoryButton.SetActive(true);
        _itemScrollRect.verticalNormalizedPosition = 1;
    }

    // Attached to on click evenet in inspector
    public void CloseEquipPanel()
    {
        _equipPanel.SetActive(false);
        _equipButton.SetActive(true);
    }

    // Attached to on click evenet in inspector
    public void ActivateAttributePanel()
    {
        _attributesPanel.SetActive(true);
        _attributesButton.SetActive(false);
    }

    // Attached to on click evenet in inspector
    public void CloseAttributePanel()
    {
        _attributesPanel.SetActive(false);
        _attributesButton.SetActive(true);
    }
    #endregion

    #region PRIVATE METHODS
    /// <summary>
    /// Activate/deactivate inventory windows.
    /// </summary>
    private void ToggleInventoryWindows()
    {
        if (InputController.Instance.ToggleInventory)
        {
            if (!_inventoryPanel.activeSelf)
                ActivateInventoryPanel();
            else
                CloseInventoryPanel();
        }
        if (InputController.Instance.ToggleEquip)
        {
            if (!_equipPanel.activeSelf)
                ActivateEquipPanel();
            else
                CloseEquipPanel();
        }
        if (InputController.Instance.ToggleAttributes)
        {
            if (!_attributesPanel.activeSelf)
                ActivateAttributePanel();
            else
                CloseAttributePanel();
        }
    }

    private void DropCurrentlySelectedItem()
    {
        // drop item if it's selected and click occurred outside UI objects 
        if (!EventSystem.current.IsPointerOverGameObject() && IsInventoryItemSelected)
        {
            GameManager.Instance.InstantiateInventoryObjectOnGround(CurrentlySelectedItem.Name);
            ResetSelectedItemReferences();
        }
    }

    private void ReturnItemToPreviousSlot()
    {        
        if (_previousItemSlot != null) 
        {
            InventorySlot slot = _previousItemSlot.GetComponent<InventorySlot>();
            slot.OnItemAdded(CurrentlySelectedItem);
            ResetSelectedItemReferences();
        }
    }

    private void PinSelectedItemToMouseArrow()
    {
        _selectedItemObjectTransform.position = Input.mousePosition;
    }

    private void CreateWarningWindow()
    {
        if (!_warningWindowObject.activeSelf)
        {
            Debug.Log("Inventory is full. Item cannot be picked up.");
            _warningWindowObject.SetActive(true);
            _warningWindow.DisplayWarningMessage();
        }
    }

    /// <summary>
    /// Reset all reference fields
    /// </summary>
    private void ResetSelectedItemReferences()
    {
        IsInventoryItemSelected = false;
        CurrentlySelectedItem = null;
        Destroy(_selectedItemObject);
        _selectedItemObject = null;
        _previousItemSlot = null;
    }
    #endregion
}
