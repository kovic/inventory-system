﻿using UnityEngine;

public class InputController : MonoBehaviour
{
    public static InputController Instance = null; 

    private void Awake()
    {
        if (Instance == null)
            Instance = this;
        else if (Instance != null)
            Destroy(gameObject);
    }

    public float HorizontalInput { get; private set; }
    public float VerticalInput { get; private set; }
    public bool ToggleInventory { get; private set; }
    public bool ToggleEquip { get; private set; }
    public bool Drop { get; private set; }
    public bool Consume { get; private set; }
    public bool ToggleAttributes { get; private set; }
    public bool SpawnNewItems { get; private set; }

    void Update ()
    {
        HorizontalInput = Input.GetAxisRaw("Horizontal");
        VerticalInput = Input.GetAxisRaw("Vertical");            
        ToggleInventory = Input.GetButtonDown("InventoryScreen");
        ToggleEquip = Input.GetButtonDown("EquipScreen");        
        Drop = Input.GetButton("Drop");
        Consume = Input.GetButtonDown("Consume");
        ToggleAttributes = Input.GetButtonDown("Attributes");
        SpawnNewItems = Input.GetButtonDown("SpawnItems");
    }
}
