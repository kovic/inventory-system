﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UiController : MonoBehaviour
{
    private Transform _transform;
    private GameObject _inventoryPanel;
    private GameObject _inventoryButton;
    private ScrollRect _itemScrollRect;

    private GameObject _equipPanel;
    private GameObject _equipButton;

    private GameObject _attributesPanel;
    private GameObject _attributesButton;

    private void Start()
    {
        _transform = transform;
        _inventoryPanel = _transform.Find("InventoryPanel").gameObject;
        _itemScrollRect = GameObject.Find("ItemScrollView").GetComponent<ScrollRect>();
        _inventoryPanel.SetActive(false);
        _inventoryButton = _transform.Find("InventoryButton").gameObject;

        _equipPanel = _transform.Find("EquipPanel").gameObject;
        _equipPanel.SetActive(false);
        _equipButton = _transform.Find("EquipButton").gameObject;

        _attributesPanel = _transform.Find("AttributesPanel").gameObject;
        _attributesPanel.SetActive(false);
        _attributesButton = _transform.Find("AttributesButton").gameObject;
    }

    private void Update()
    {
        if (InputController.Instance.ToggleInventory)
        {
            if (!_inventoryPanel.activeSelf)
                ActivateInventoryPanel();
            else
                CloseInventoryPanel();
        }
        if (InputController.Instance.ToggleEquip)
        {
            if (!_equipPanel.activeSelf)
                ActivateEquipPanel();
            else
                CloseEquipPanel();
        }
        if (InputController.Instance.ToggleAttributes)
        {
            if (!_attributesPanel.activeSelf)
                ActivateAttributePanel();
            else
                CloseAttributePanel();
        }
    }

    public bool IsInventoryClosed()
    {
        return !_inventoryPanel.activeSelf && !_equipPanel.activeSelf;
    }

    // TODO: When panel is activated, button needs to be deactivated. Need to think of a smart way to implement this. Problem is that the function can take a single parameter so I can't pass in both panel and button.
    // Only thing I can think of right now i so somehow parse panel name and then affect the button that shares the same prefix (Inventory or Equip). But that's totally hacky!
    // Try to handle this with two functions, opening and closing function and just pass in the panel and the button
    // There's a cool way of handling stuff like this. Parameter of SetActive should be negative property of its current state (!activeSelf)
    public void ActivateInventoryPanel()
    {
        _inventoryPanel.SetActive(true);
        _inventoryButton.SetActive(false);
    }

    public void ActivateEquipPanel()
    {
        _equipPanel.SetActive(true);
        _equipButton.SetActive(false);
    }

    public void CloseInventoryPanel()
    {
        _inventoryPanel.SetActive(false);
        _inventoryButton.SetActive(true);
        _itemScrollRect.verticalNormalizedPosition = 1;
    }

    public void CloseEquipPanel()
    {
        _equipPanel.SetActive(false);
        _equipButton.SetActive(true);
    }

    public void ActivateAttributePanel()
    {
        _attributesPanel.SetActive(true);
        _attributesButton.SetActive(false);
    }

    public void CloseAttributePanel()
    {
        _attributesPanel.SetActive(false);
        _attributesButton.SetActive(true);
    }
}
