﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public Dictionary<AttributeType, int> AttributeList;

    [SerializeField] private int _strength = 1;
    [SerializeField] private int _dexterity = 1;
    [SerializeField] private int _agility = 1;
    [SerializeField] private int _intelligence = 1;
    [SerializeField] private float _speed = 1.0f;

    private float _horizontal;
    private float _vertical;
    private Transform _transform;
    private Vector2 _movementDirection;
    private Vector2 _targetPosition;
    private Rigidbody2D _rigidbody;
    private Animator _animator;
    private SpriteRenderer _renderer;

    private void Awake()
    {
        _rigidbody = GetComponent<Rigidbody2D>();
        _transform = transform;
        _animator = GetComponent<Animator>();
        _renderer = GetComponent<SpriteRenderer>();
        AttributeList = new Dictionary<AttributeType, int>();
        InstantiateAttributeValues();
    }
	
	void FixedUpdate ()
    {
        Move();
    }

    void Move()
    {
        _horizontal = InputController.Instance.HorizontalInput;
        _vertical = InputController.Instance.VerticalInput;
        _movementDirection = (new Vector2(_horizontal, _vertical)).normalized * _speed * Time.deltaTime;
        _targetPosition = (Vector2)_transform.position + _movementDirection;
        _rigidbody.MovePosition(_targetPosition);

        // Set animator fields
        _animator.SetFloat("MoveY", _vertical);
        _animator.SetBool("MoveX", _horizontal != 0);
        if (_horizontal < 0)
            _renderer.flipX = true;
        else
            _renderer.flipX = false;
    }
   
    public void InstantiateAttributeValues()
    {
        AttributeList.Add(AttributeType.Agility, _agility);
        AttributeList.Add(AttributeType.Dexterity, _dexterity);
        AttributeList.Add(AttributeType.Intelligence, _intelligence);
        AttributeList.Add(AttributeType.Strength, _strength);
    }

    // Determines collision with items 
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Item"))
        {
            InteractableObject interactableObject = collision.transform.GetComponent<InteractableObject>();
            // If item is collestible, pick it up, if not, use it
            if (interactableObject.Item.IsCollectable)
                interactableObject.PickUp();
            else
                interactableObject.Use();
        }
    }

}
