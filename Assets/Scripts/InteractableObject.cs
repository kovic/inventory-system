﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class InteractableObject : MonoBehaviour
{
    public InventoryItem Item;

    public void PickUp()
    {
        // Check with Inventory controller if item can be picked up
        bool canPickUp = InventoryController.Instance.AddItemToInventory(Item);
        if (canPickUp)
            Destroy(gameObject);
        else
            return;
    }

    public void Use()
    {
        Item.Use();
        Destroy(gameObject);
    }
}
