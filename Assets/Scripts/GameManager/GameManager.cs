﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance = null;
    public List<GameObject> SpawnableItems = new List<GameObject>();
    public Transform spawnLocation;
    [HideInInspector] public Transform Player;

    private float _itemDropPointOffsetY = 1.5f;

    private void Awake()
    {
        if (Instance == null)
            Instance = this;
        else if (Instance != null)
            Destroy(gameObject);

        Player = GameObject.FindGameObjectWithTag("Player").transform;
    }

    private void Update()
    {
        if (InputController.Instance.SpawnNewItems)
            SpawnItems();
    }

    /// <summary>
    /// Drops inventory item on the ground.
    /// </summary>
    /// <param name="itemName">Name of the item that is dropped from inventory.</param>
    public void InstantiateInventoryObjectOnGround(string itemName)
    {
        foreach (GameObject spawnableItem in SpawnableItems)
        {
            if (spawnableItem.name == itemName)
            {
                Vector2 playerTransform = Player.position;
                Vector2 discardLocation = playerTransform + new Vector2(_itemDropPointOffsetY, 0f);
                Instantiate(spawnableItem, discardLocation, Quaternion.identity);
                return;
            }
        }
    }

    private void SpawnItems()
    {
        foreach(GameObject spawnableItem in SpawnableItems)
        {
            Instantiate(spawnableItem, spawnLocation);
        }
    }    
}
