﻿using UnityEngine;

[CreateAssetMenu(fileName = "non-Stackable Item", menuName = "Inventory Items/Non-Stackable Item", order = 4)]
public class NonStackableItem : InventoryItem 
{

}
