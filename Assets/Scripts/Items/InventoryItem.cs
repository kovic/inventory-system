﻿using UnityEngine;

[CreateAssetMenu(fileName = "Inventory Item", menuName = "Inventory Items/Base Item", order = 1)]
public class InventoryItem : ScriptableObject
{
    public string Name;
    public Sprite Icon;
    public bool IsCollectable;
    public bool IsConsumable;
    public virtual void Use()
    {
        Debug.Log(string.Format("Item {0} was used!", Name));
    }
    public virtual void DropToSlot()
    {
        Debug.Log("DropToSlot function not implemented fot this item!");
    }
}
