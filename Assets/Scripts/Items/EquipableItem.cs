﻿using UnityEngine;

[CreateAssetMenu(fileName = "Equipable Item", menuName = "Inventory Items/Equipable Item", order = 2)]
public class EquipableItem : InventoryItem 
{
    public EquipmentType Type;
    public int Defense;
    public int Attack;
    public AttributeType Attribute;
    public float MaxDurability;
}
