﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum EquipmentType 
{
    Armor,
    Shield,
    Weapon,
    Helmet
}
