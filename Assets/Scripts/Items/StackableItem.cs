﻿using UnityEngine;

[CreateAssetMenu(fileName = "Stackable Item", menuName = "Inventory Items/Stackable Item", order = 3)]
public class StackableItem : InventoryItem 
{
    public bool HasLimit;
    public int MaxLimit;
}
